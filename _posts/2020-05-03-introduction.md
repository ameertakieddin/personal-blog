---
layout: post
title: "Welcome!"
date: 2020-05-03
description: "Introduction To The Blog"
image: /assets/images/introduction/dandelion.jpg
author: Ameer Takieddin
tags: 
  - Introduction
---
Hi and Welcome to my blog! 

My name is Ameer Takieddin and I'm a developer in the Northern Virginia Area.
I've previously graduated from George Mason University with a degree in Computer Science and I am currently attending Georgia Tech's Online Master's of Computer Science (OMSCS) for Machine Learning.

Along with pursuing a degree in Machine Learning, my other interests are within in the distributed systems space, including things like Kubernetes or more P2P technology, computer vision, and full stack development.

In my free time I'm typically coding up something to run on a raspberry pi I have laying around (I recently got the Voice HAT shown below but I haven't had the time to properly set it up yet) or playing around with some new concept I recently learned, which I'm hoping to start sharing here!

![Placeholder](/personal-blog/assets/images/introduction/pi.jpg)


I also enjoy photography, and will show case some of the photos I take here as well. I'm currently shooting with a Sony a6000 when I can (remember) to take it with me, but when I forget I'm also running around with a Pixel 3 XL.

![Placeholder](/personal-blog/assets/images/introduction/metro.jpg#full)

All photos will be from me (unless otherwise cited).

Hope you enjoy!
