---
layout: post
title: "Monte Carlo"
date: 2020-05-23
description: "Or How Randominess Leads To Predictability (Part One: Estimating Distributions)"
image: /assets/images/monte-carlo/dice.jpg
author: Ameer Takieddin
tags: 
  - Monte Carlo
---

### Introduction

There was is an episode of Black Mirror called Hang the DJ, which if you haven't seen it, it's a good episode and I recommend it (also spoilers below, so if you care about it I highly recommend watching it before continuing reading)

The basic premise is simple, people live in a walled community and get matched with potential partners for a specified amount of time, until their ultimate match is found. We follow two people as they fall for each, and eventually plan their escape from the community [1].

![Placeholder](/personal-blog/assets/images/monte-carlo/hang_the_dj.jpg)



As our two main characters escape the community, it is revealed however that what the viewer just watched was actually one of a million simulations ran by an app. Each simulation changed one or two things, but ultimately accumulated into one final score for our two protagonists in the real world.

At the time of watching, I didn't really make the connection to Monte Carlo methods, but in actuality that's what was happening.

### Definition
From *Wikipedia*
>*Monte Carlo methods, or Monte Carlo experiments, are a broad class of computational algorithms that rely on repeated random sampling to obtain numerical results.*

In practice the term "Monte Carlo" is used pretty liberally. There are Monte Carlo approaches, algorithms, and methods, but all at it's core leverages the idea that with enough simulated randominess or samples, we can predict something with much more accuracy than mere averages.

In the case of the Black Mirror episode, the numerical result was the compatibility of the two users, but in real world, Monte Carlo methods were used to simulate a wide range of complex scenarios, and in one case a rather explosive one [2].

But what if your problem is much more down to earth? After all not everyone is planning on building dating apps or weapons of mass destruction.

For our exploration into the application of Monte Carlo, let us first define a problem.

### Problem Description

The easiest (and least interesting) way to view how Monte Carlo methods work is by probability distribution estimating of random variables. 

The idea is that if we have some random variable with an unknown probability distribution that we want to map, we can use a Monte Carlo approach to essentially estimate how that variables distribution looks like.

In short, by taking more and more returns of a function, we can approximate the function's distribution.

We can see examples of this below, with two different distributions [3].

![Placeholder](/personal-blog/assets/images/monte-carlo/binomial.png)


![Placeholder](/personal-blog/assets/images/monte-carlo/normal.png)


This is all fine and dandy, but what would this look like in a more complex environment, say getting an agent to navigate a gridworld.

In the next post in the series, we'll explore what happens when we use a similar approach in guiding/evaluating how well an agent can traverse a gridworld.

---
## Sources

[1] Black Mirror image: https://pyxis.nymag.com/v1/imgs/4e8/257/3f2057200e9d835e2bb531aef772ebf563-10-hang-the-dj.2x.rhorizontal.w700.jpg

[2] https://en.wikipedia.org/wiki/Monte_Carlo_method#History

[3] Example adopted from: https://machinelearningmastery.com/monte-carlo-sampling-for-probability/
