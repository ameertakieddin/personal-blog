![Build Status](https://gitlab.com/pages/jekyll/badges/master/build.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---
A repo to contain all the contents and assets of my personal blog

This blog's style was heavily inspired from https://github.com/samesies/barber-jekyll